package com.example.dell.app.db;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.dell.app.db.model.RouteEntity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class FirebaseHelper {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference database;
    private FirebaseUser firebaseUser;

    public FirebaseHelper() {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        database = FirebaseDatabase.getInstance().getReference("routes").child(firebaseUser.getUid());
    }

    public FirebaseUser getCurrentUser() {
        return firebaseAuth.getCurrentUser();
    }

    public void insertRoute(RouteEntity routes, long routeId) {
        routes.setId(routeId);
        database.child(String.valueOf(routeId)).setValue(routes);
    }

    public void removeRoute(long id) {
        database.child(String.valueOf(id)).removeValue();
    }

    public Observable<Float> getMaxDistance() {
        return Observable.create(subscriber ->
                database.orderByChild("distance").limitToLast(1).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot singleDataSnapshot : dataSnapshot.getChildren()) {
                            RouteEntity routeEntity = singleDataSnapshot.getValue(RouteEntity.class);
                            Float distance;
                            if (routeEntity != null) {
                                distance = routeEntity.getDistance();
                            } else {
                                distance = 0.0f;
                            }
                            if (!subscriber.isDisposed()) {
                                subscriber.onNext(distance);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                }));
    }

    public Observable<Integer> getMaxSpeed() {
        return Observable.create(subscriber ->
                database.orderByChild("maxSpeed").limitToLast(1).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot singleDataSnapshot : dataSnapshot.getChildren()) {
                            RouteEntity routeEntity = singleDataSnapshot.getValue(RouteEntity.class);
                            int speed;
                            if (routeEntity != null) {
                                speed = routeEntity.getMaxSpeed();
                            } else {
                                speed = 0;
                            }
                            if (!subscriber.isDisposed()) {
                                subscriber.onNext(speed);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                }));
    }

    public Observable<Long> getMaxTime() {
        return Observable.create(subscriber ->
                database.orderByChild("time").limitToLast(1).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot singleDataSnapshot : dataSnapshot.getChildren()) {
                            RouteEntity routeEntity = singleDataSnapshot.getValue(RouteEntity.class);
                            long time;
                            if (routeEntity != null) {
                                time = routeEntity.getTime();
                            } else {
                                time = 0L;
                            }
                            if (!subscriber.isDisposed()) {
                                subscriber.onNext(time);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                }));
    }

    public Observable<List<RouteEntity>> getRoutes() {
        return Observable.create(subscriber ->
                database.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        List<RouteEntity> routeEntities = new ArrayList<>();
                        for (DataSnapshot singleDataSnapshot : dataSnapshot.getChildren()) {
                            RouteEntity rootNodeObject = singleDataSnapshot.getValue(RouteEntity.class);
                            routeEntities.add(rootNodeObject);
                        }
                        if (!subscriber.isDisposed()) {
                            subscriber.onNext(routeEntities);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        if (!subscriber.isDisposed()) {
                            Log.d("Firebase", databaseError.getMessage());
                        }
                    }
                }));
    }


}
