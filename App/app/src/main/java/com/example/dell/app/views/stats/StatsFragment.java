package com.example.dell.app.views.stats;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.app.R;
import com.example.dell.app.helpers.DateHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatsFragment extends Fragment {

    private StatsViewModel viewModel;

    @BindView(R.id.maxSpeedTV)
    TextView maxSpeedTV;
    @BindView(R.id.maxDistanceTV)
    TextView maxDistanceTV;
    @BindView(R.id.maxTimeTV)
    TextView maxTimeTV;

    private boolean checked = false;
    private Toast toast = null;

    public static StatsFragment newInstance() {
        StatsFragment fragment = new StatsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_stats, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        viewModel = ViewModelProviders.of(this).get(StatsViewModel.class);
        setup();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.menuSwitch);
        item.setActionView(R.layout.switch_layout);
        Switch switchAB = item
                .getActionView().findViewById(R.id.menuSwitch);
        switchAB.setOnCheckedChangeListener((buttonView, isChecked) -> {
            checked = isChecked;
            viewModel.getMax(checked);
            if (isChecked) {
                makeToast("Firebase");
            } else {
                makeToast("Local");
            }
        });
    }

    private void makeToast(String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
                .show();
    }

    private void setup() {
        viewModel.getMax(checked);
        viewModel.getDistance().observe(this, distance -> maxDistanceTV.setText(String.format("%.2f", distance) + " km"));
        viewModel.getSpeed().observe(this, speed -> maxSpeedTV.setText(speed + " km/h"));
        viewModel.getTime().observe(this, time -> maxTimeTV.setText("" + DateHelper.convertTime(time)));
    }
}
