package com.example.dell.app.helpers;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DateHelper {
    public static final String convertTime(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(time);
    }
}
