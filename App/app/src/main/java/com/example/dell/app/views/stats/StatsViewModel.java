package com.example.dell.app.views.stats;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.dell.app.base.BaseViewModel;
import com.example.dell.app.db.Repository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class StatsViewModel extends BaseViewModel {

    private final Repository repository;

    private MutableLiveData<Integer> speed = new MutableLiveData<>();
    private MutableLiveData<Float> distance = new MutableLiveData<>();
    private MutableLiveData<Long> time = new MutableLiveData<>();

    public MutableLiveData<Float> getDistance() {
        return distance;
    }

    public MutableLiveData<Long> getTime() {
        return time;
    }

    public MutableLiveData<Integer> getSpeed() {
        return speed;
    }

    public StatsViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application.getBaseContext());
    }

    private void getMaxSpeedFirebase() {
        disposables.add(repository.getMaxSpeedFromFirebase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        speed.setValue(it)));
    }

    public void getMaxDistanceFirebase() {
        disposables.add(repository.getMaxDistanceFromFirebase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        distance.setValue(it)));
    }

    private void getMaxTimeFirebase() {
        disposables.add(repository.getMaxTimeFromFireBase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        time.setValue(it)));
    }

    private void getMaxSpeed() {
        disposables.add(repository.getMaxSpeed()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        speed.setValue(it)));
    }

    public void getMaxDistance() {
        disposables.add(repository.getMaxDistance()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        distance.setValue(it)));
    }

    private void getMaxTime() {
        disposables.add(repository.getMaxTime()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        time.setValue(it)));
    }


    public void getMax(boolean checked) {
        if (checked) {
            getMaxSpeedFirebase();
            getMaxDistanceFirebase();
            getMaxTimeFirebase();
        } else {
            getMaxSpeed();
            getMaxDistance();
            getMaxTime();
        }
    }
}

