package com.example.dell.app.views.routes;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.dell.app.base.BaseViewModel;
import com.example.dell.app.db.Repository;
import com.example.dell.app.db.model.RouteEntity;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RoutesViewModel extends BaseViewModel {

    private final Repository repository;
    private MutableLiveData<List<RouteEntity>> routes = new MutableLiveData<>();

    public MutableLiveData<Boolean> getIsDeleted() {
        return isDeleted;
    }

    private MutableLiveData<Boolean> isDeleted = new MutableLiveData<>();

    public MutableLiveData<List<RouteEntity>> getRoutes() {
        return routes;
    }

    public RoutesViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application.getBaseContext());
    }

    public void getAllLocalRoutes() {
        disposables.add(repository.getRoutes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        routes.setValue(it)));
    }

    public void getAllRoutes(boolean checked) {
        if (checked) {
            getAllFirebaseRoutes();
        } else {
            getAllLocalRoutes();
        }
    }

    public void removeRoute(boolean checked, long routeId) {
        if (checked) {
            removeFirebaseRoute(routeId);
        } else {
            removeLocalRoute(routeId);
        }
    }

    private void removeLocalRoute(long routeId) {
        disposables.add(repository.deleteRoute(routeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(it -> isDeleted.setValue(true))
                .doOnError(it -> isDeleted.setValue(false))
                .subscribe());
    }

    private void removeFirebaseRoute(long routeId) {
        disposables.add(repository.deleteRouteFirebase(routeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(it -> isDeleted.setValue(true))
                .doOnError(it -> isDeleted.setValue(false))
                .subscribe());
    }

    public void getAllFirebaseRoutes() {
        disposables.add(repository.getRoutesFromFirebase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it ->
                        routes.setValue(it)));
    }


}
