package com.example.dell.app.views.track;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.dell.app.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrackActivity extends AppCompatActivity implements OnMapReadyCallback {


    private GoogleMap map;
    private TrackViewModel viewModel;
    private Polyline polyline = null;
    private PolylineOptions polylineOptions = null;
    private MapLocationSource mapLocationSource = new MapLocationSource();
    private static int ZOOM = 14;
    private AlertDialog.Builder dialog = null;

    @BindView(R.id.distanceTV)
    protected TextView distanceTV;
    @BindView(R.id.speedTV)
    protected TextView speedTV;
    @BindView(R.id.timeTV)
    protected TextView timeTV;
    @BindView(R.id.finishTrack)
    protected FloatingActionButton finishTrackFAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(TrackViewModel.class);
        checkPermissions();
    }


    @OnClick(R.id.finishTrack)
    protected void finishTrackClicked() {
        onBackPressed();
    }

    private void setupMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setup() {
        viewModel.locationPermissionGranted();
        setupMovableMap();
        map.setLocationSource(mapLocationSource);
        showMyLocation();
        setupLocation();
    }

    private void setupLocation() {
        viewModel.locationPermissionGranted();
        viewModel.getLocation().observe(this, location -> {
            mapLocationSource.updateLocation(location);
            drawPolyLine(location);
            setCameraPosition(new LatLng(location.getLatitude(), location.getLongitude()));
        });
        viewModel.getDistance().observe(this, distance -> {
            distanceTV.setText(String.format("%.2f", distance) + "km");
        });
        viewModel.getActualSpeed().observe(this, speed -> {
            speedTV.setText(speed + "km/h");
        });
        viewModel.getRouteTime().observe(this, time -> {
            timeTV.setText(time);
        });
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            finish();
        } else {
            setupMap();
        }
    }

    public void drawPolyLine(Location location) {
        drawPolyLine(location.getLatitude(), location.getLongitude());
    }

    public void drawPolyLine(double lat, double lon) {
        if (polyline != null) {
            polyline.remove();
        }
        if (polylineOptions == null) {
            setupPolyLineOptions();
        }
        polylineOptions.add(new LatLng(lat, lon));
        polyline = map.addPolyline(polylineOptions);
    }

    private void setupPolyLineOptions() {
        polylineOptions = new PolylineOptions();
        polylineOptions.width(15);
        polylineOptions.color(getColor(R.color.colorPrimaryDark));
        polylineOptions.visible(true);
    }

    @SuppressLint("MissingPermission")
    private void showMyLocation() {
        map.setMyLocationEnabled(true);
        updateCameraZoom();
    }

    private void setCameraPosition(LatLng position) {
        if (map != null) {
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(position, ZOOM);
            map.animateCamera(update);
        }
    }

    private void updateCameraZoom() {
        final CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM);
        map.animateCamera(zoom);
    }


    public void setupMovableMap() {
        map.getUiSettings().setScrollGesturesEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        setup();
    }


    @Override
    public void onBackPressed() {
        viewModel.getSaved().observe(this, saved -> {
            if (saved) {
                super.onBackPressed();
            }
        });

        if (dialog == null) {
            buildAlertDialog();
            dialog.show();
        } else {
            dialog.show();
        }
    }

    private AlertDialog buildAlertDialog() {
        dialog = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        dialog.setMessage(R.string.alert_text)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        viewModel.saveRoute();
                        dialog.dismiss();
                        onBackPressed();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        return dialog.create();
    }
}
