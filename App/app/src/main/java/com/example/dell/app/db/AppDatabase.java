package com.example.dell.app.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.dell.app.db.dao.LocationsDao;
import com.example.dell.app.db.dao.RouteDao;
import com.example.dell.app.db.model.LocationsEntity;
import com.example.dell.app.db.model.RouteEntity;

@Database(entities = {LocationsEntity.class, RouteEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract LocationsDao locationDao();

    public abstract RouteDao routeDao();

    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "map_database")
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
