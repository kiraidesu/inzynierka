package com.example.dell.app.views.startTrack;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.dell.app.R;
import com.example.dell.app.views.track.TrackActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartTrackFragment extends Fragment {

    @BindView(R.id.startButton)
    ImageButton startButton;

    public static StartTrackFragment newInstance() {
        StartTrackFragment fragment = new StartTrackFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track, parent, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.startButton)
    protected void setStartButtonClicked() {
        Intent intent = new Intent(getActivity(), TrackActivity.class);
        startActivity(intent);
    }
}
