package com.example.dell.app.views.register;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.dell.app.base.BaseViewModel;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterViewModel extends BaseViewModel {

    private FirebaseAuth firebaseAuth;

    private MutableLiveData<Boolean> isRegistered = new MutableLiveData<>();

    public MutableLiveData<Boolean> getIsRegistered() {
        return isRegistered;
    }

    public RegisterViewModel(@NonNull Application application) {
        super(application);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void RegisterAccount(String email, String password) {
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        isRegistered.setValue(true);
                    } else {
                        isRegistered.setValue(false);
                    }
                });
    }
}
