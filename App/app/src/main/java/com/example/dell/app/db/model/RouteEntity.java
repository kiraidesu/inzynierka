package com.example.dell.app.db.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "routes")
public class RouteEntity {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private float distance; //in km
    private int maxSpeed;
    private long time;

    public RouteEntity(){}

    public RouteEntity(float distance, int maxSpeed, long time) {
        this.distance = distance;
        this.maxSpeed = maxSpeed;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
