package com.example.dell.app.db;

import android.content.Context;

import com.example.dell.app.db.dao.LocationsDao;
import com.example.dell.app.db.dao.RouteDao;
import com.example.dell.app.db.model.LocationsEntity;
import com.example.dell.app.db.model.RouteEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;

public class Repository {
    private final LocationsDao locationsDao;
    private final AppDatabase appDatabase;
    private final RouteDao routeDao;
    private final FirebaseHelper firebaseHelper;

    public Repository(Context context) {
        appDatabase = AppDatabase.getAppDatabase(context);
        locationsDao = appDatabase.locationDao();
        routeDao = appDatabase.routeDao();
        firebaseHelper = new FirebaseHelper();
    }

    public Single<List<RouteEntity>> getRoutes() {
        return routeDao.getAllRoutes();
    }

    public Single<List<LocationsEntity>> getRouteWithLocationsById(int routeId) {
        return locationsDao.getRouteLocations(routeId);
    }

    public Single<Integer> getMaxSpeed() {
        return routeDao.getMaxSpeed();
    }

    public Single<Float> getMaxDistance() {
        return routeDao.getMaxDistance();
    }

    public Single<Long> getMaxTime() {
        return routeDao.getMaxTime();
    }

    public Single<Long> insertRoute(RouteEntity route) {
        return Single.create((SingleEmitter<Long> emitter) -> {
            emitter.onSuccess(routeDao.insertRoute(route));
        });
    }

    public Completable insertLocations(List<LocationsEntity> locations) {
        return Completable.fromAction(() -> locationsDao.insertLocations(locations));
    }

    public Completable insertLocation(LocationsEntity location) {
        return Completable.fromAction(() -> locationsDao.insertLocation(location));
    }

    public Completable insertLocationToFirebase(RouteEntity route, long routeId) {
        return Completable.fromAction(() -> firebaseHelper.insertRoute(route, routeId));
    }

    public Observable<List<RouteEntity>> getRoutesFromFirebase() {
        return firebaseHelper.getRoutes();
    }

    public Observable<Float> getMaxDistanceFromFirebase() {
        return firebaseHelper.getMaxDistance();
    }

    public Observable<Long> getMaxTimeFromFireBase() {
        return firebaseHelper.getMaxTime();
    }

    public Observable<Integer> getMaxSpeedFromFirebase() {
        return firebaseHelper.getMaxSpeed();
    }

    public Single<Integer> deleteRoute(long id) {
        return Single.create((SingleEmitter<Integer> emitter) -> emitter.onSuccess(routeDao.deleteRoute(id)));
    }

    public Single<Integer> deleteRouteFirebase(long routeId) {
        return Single.create((SingleEmitter<Integer> emitter) -> firebaseHelper.removeRoute(routeId));
    }
}
