package com.example.dell.app.views.main;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.dell.app.base.BaseViewModel;
import com.example.dell.app.db.FirebaseHelper;

public class MainViewModel extends BaseViewModel {

    FirebaseHelper firebaseHelper = new FirebaseHelper();
    MutableLiveData<Boolean> isNotLogged = new MutableLiveData<>();
    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    public void checkIsUserLogged(){
        if(firebaseHelper.getCurrentUser() == null){
            isNotLogged.setValue(true);
        }else{
            isNotLogged.setValue(false);
        }
    }

}
