package com.example.dell.app.views.login;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.app.R;
import com.example.dell.app.views.main.MainActivity;
import com.example.dell.app.views.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    LoginViewModel viewModel;
    Toast toast = null;

    @BindView(R.id.email)
    protected EditText emailEt;
    @BindView(R.id.password)
    protected EditText passwordEt;
    @BindView(R.id.btn_reg)
    protected TextView regButton;
    @BindView(R.id.btn_login)
    protected Button loginButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        setup();
    }

    private void setup() {
        viewModel.getIsLogged().observe(this, isLogged -> {
            if (isLogged) {
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                createToast(getString(R.string.login_failed));
            }
        });
    }

    @OnClick(R.id.btn_login)
    protected void loginClicked() {
        String email = emailEt.getText().toString();
        String password = passwordEt.getText().toString();
        if (!email.isEmpty() && !password.isEmpty()) {
            viewModel.loginAccount(email, password);
        } else {
            createToast(getString(R.string.register_info));
        }
    }

    private void createToast(String message) {
        if (toast != null) {
            toast.cancel();
        } else {
            toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_reg)
    protected void regClicked() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
