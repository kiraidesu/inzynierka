package com.example.dell.app.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.dell.app.db.model.RouteEntity;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface RouteDao {
    @Query("SELECT * FROM routes")
    Single<List<RouteEntity>> getAllRoutes();

    @Query("SELECT COALESCE(MAX(maxSpeed),0) FROM routes")
    Single<Integer> getMaxSpeed();

    @Query("SELECT COALESCE(MAX(distance),0) FROM routes")
    Single<Float> getMaxDistance();

    @Query("SELECT COALESCE(MAX(time),0) FROM routes")
    Single<Long> getMaxTime();

    @Insert
    long insertRoute(RouteEntity route);

    @Query("DELETE FROM routes WHERE id =:id")
    int deleteRoute(long id);
}
