package com.example.dell.app.views.startTrack;

import android.app.Application;
import android.support.annotation.NonNull;

import com.example.dell.app.base.BaseViewModel;

public class StartTrackViewModel extends BaseViewModel {

    public StartTrackViewModel(@NonNull Application application) {
        super(application);
    }
}
