package com.example.dell.app.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.location.Location;

@Entity(tableName = "locations")
public class LocationsEntity {
    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "route_id")
    private long routeId;

    private double lat;

    private double lon;

    public LocationsEntity(long routeId, double lat, double lon) {
        this.routeId = routeId;
        this.lat = lat;
        this.lon = lon;
    }

    public static LocationsEntity mapLocationsEntity(Location location, long routeId) {
        return new LocationsEntity(
                routeId,
                location.getLatitude(),
                location.getLongitude()
        );
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
}
