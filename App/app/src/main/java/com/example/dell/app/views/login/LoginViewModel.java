package com.example.dell.app.views.login;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.dell.app.base.BaseViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginViewModel extends BaseViewModel {

    private FirebaseAuth firebaseAuth;

    private MutableLiveData<Boolean> isLogged = new MutableLiveData<>();

    public MutableLiveData<Boolean> getIsLogged() {
        return isLogged;
    }

    public LoginViewModel(@NonNull Application application) {
        super(application);
        firebaseAuth = FirebaseAuth.getInstance();
        checkIsLogged();
    }

    private void checkIsLogged() {
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser != null) {
            isLogged.setValue(true);
        }
    }

    public void loginAccount(String email, String password) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            isLogged.setValue(true);
                        } else {
                            isLogged.setValue(false);
                        }
                    }
                });
    }
}
