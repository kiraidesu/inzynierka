package com.example.dell.app.views.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Switch;

import com.example.dell.app.R;
import com.example.dell.app.views.login.LoginActivity;
import com.example.dell.app.views.routes.RoutesFragment;
import com.example.dell.app.views.startTrack.StartTrackFragment;
import com.example.dell.app.views.stats.StatsFragment;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.bottomNavView)
    protected BottomNavigationView bottomNavigationView;

    private MainViewModel viewModel;
    private Fragment selectedFragment = null;
    private AlertDialog.Builder dialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        setup();
    }

    private void setup() {
        selectedFragment = StartTrackFragment.newInstance();
        changeFragment();

        viewModel.checkIsUserLogged();
        viewModel.isNotLogged.observe(this, isNotLogged -> {
            if (isNotLogged) {
                openLoginActivity();
            }
        });

        setupBottomNavBar();
    }

    private void setupBottomNavBar() {
        bottomNavigationView.setOnNavigationItemSelectedListener
                (item -> {
                    switch (item.getItemId()) {
                        case R.id.navigation_track:
                            selectedFragment = StartTrackFragment.newInstance();
                            break;
                        case R.id.navigation_routes:
                            selectedFragment = RoutesFragment.newInstance();
                            break;
                        case R.id.navigation_stats:
                            selectedFragment = StatsFragment.newInstance();
                            break;
                    }
                    changeFragment();
                    return true;
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.signOut) {
            if (dialog == null) {
                buildAlertDialog();
                dialog.show();
            } else {
                dialog.show();
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.menuSwitch);
        item.setActionView(R.layout.switch_layout);
        Switch switchAB = item
                .getActionView().findViewById(R.id.menuSwitch);

        return true;

    }

    @Override
    public MenuInflater getMenuInflater() {
        return super.getMenuInflater();
    }

    private void changeFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentContainer, selectedFragment);
        transaction.commit();
    }

    private AlertDialog buildAlertDialog() {
        dialog = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        dialog.setMessage(R.string.dialog_logout_text)
                .setPositiveButton(R.string.yes, (dialog, id) -> logout())
                .setNegativeButton(R.string.no, (dialog, id) -> dialog.dismiss());
        return dialog.create();
    }

    private void logout() {
        FirebaseAuth.getInstance().signOut();
        openLoginActivity();
        finish();
    }

    private void openLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}