package com.example.dell.app.views.register;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dell.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    RegisterViewModel viewModel;
    Toast toast = null;

    @BindView(R.id.password)
    protected EditText passwordEt;
    @BindView(R.id.email)
    protected EditText emailEt;
    @BindView(R.id.btn_reg)
    protected Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        setup();
    }

    private void setup() {
        viewModel.getIsRegistered().observe(this, isRegistered -> {
            if (isRegistered) {
                createToast(getString(R.string.register_success));
            } else {
                createToast(getString(R.string.register_fail));
            }
        });
    }

    @OnClick(R.id.btn_reg)
    protected void registerClicked() {
        String email = emailEt.getText().toString();
        String password = passwordEt.getText().toString();
        if (!email.isEmpty() && !password.isEmpty()) {
            viewModel.RegisterAccount(email, password);
        } else {
            createToast(getString(R.string.register_info));
        }
    }

    private void createToast(String message) {
        if (toast != null) {
            toast.cancel();
        } else {
            toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }
}
