package com.example.dell.app.views.routes;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.example.dell.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoutesFragment extends Fragment {

    @BindView(R.id.routesRV)
    RecyclerView routesRV;

    RoutesViewModel viewModel;
    Toast toast = null;
    private boolean checked = false;

    private final RoutesAdapter adapter = new RoutesAdapter();

    public static RoutesFragment newInstance() {
        RoutesFragment fragment = new RoutesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_routes, parent, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.menuSwitch);
        item.setActionView(R.layout.switch_layout);
        Switch switchAB = item
                .getActionView().findViewById(R.id.menuSwitch);

        switchAB.setOnCheckedChangeListener((buttonView, isChecked) -> {
            checked = isChecked;
            viewModel.getAllRoutes(checked);
            if (isChecked) {
                makeToast("Firebase");
            } else {
                makeToast("Local");
            }
        });
    }

    private void makeToast(String text) {
        if (toast != null) {
            toast.cancel();
        }
        toast.makeText(getContext(), text, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(RoutesViewModel.class);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        setup();
    }

    private void setup() {
        routesRV.setAdapter(adapter);
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                viewModel.removeRoute(checked, adapter.getRoutes().get(viewHolder.getAdapterPosition()).getId());
                adapter.getRoutes().remove(viewHolder.getAdapterPosition());
                adapter.notifyDataSetChanged();
            }
        }).attachToRecyclerView(routesRV);

        routesRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        routesRV.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        viewModel.getAllRoutes(checked);
        viewModel.getRoutes().observe(this, routes -> {
            adapter.setRoutes(routes);
            adapter.notifyDataSetChanged();
        });
        viewModel.getIsDeleted().observe(this, isDeleted -> {
            if (isDeleted) {
                makeToast(getString(R.string.deleted));
            } else {
                makeToast(getString(R.string.not_deleted));
            }
        });
    }

}
