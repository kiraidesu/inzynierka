package com.example.dell.app.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.dell.app.db.model.LocationsEntity;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface LocationsDao {

    @Query("SELECT * FROM locations WHERE route_id = :routeId")
    Single<List<LocationsEntity>> getRouteLocations(long routeId);

    @Insert
    void insertLocation(LocationsEntity location);

    @Insert
    void insertLocations(List<LocationsEntity> locations);
}

