package com.example.dell.app.views.routes;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dell.app.R;
import com.example.dell.app.db.model.RouteEntity;
import com.example.dell.app.helpers.DateHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoutesAdapter extends RecyclerView.Adapter<RoutesAdapter.ViewHolder> {

    private RecyclerView recyclerView;

    public List<RouteEntity> getRoutes() {
        return routes;
    }

    private List<RouteEntity> routes = new ArrayList<>();

    public void setRoutes(List<RouteEntity> routes) {
        this.routes = routes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_routes, parent, false);
        return new RoutesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RoutesAdapter.ViewHolder holder, int position) {
        final RouteEntity route = routes.get(position);
        holder.bind(position, route.getDistance(), route.getMaxSpeed(), route.getTime());
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return routes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.timeTV)
        TextView timeTV;
        @BindView(R.id.speedTV)
        TextView speedTV;
        @BindView(R.id.distanceTV)
        TextView distanceTV;
        @BindView(R.id.routeTV)
        TextView routeTV;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(int position, float distance, int maxSpeed, long time) {
            routeTV.setText(recyclerView.getContext().getResources().getString(R.string.route_name_number, position + 1));
            timeTV.setText("" + DateHelper.convertTime(time));
            speedTV.setText(maxSpeed + " km/h");
            distanceTV.setText(String.format("%.2f", distance) + " km"); //todo poprawić drogę, by zaokrąglało do któregoś po przecinkiem
        }
    }
}
