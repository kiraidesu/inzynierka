package com.example.dell.app.views.track;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.location.Location;
import android.support.annotation.NonNull;

import com.example.dell.app.base.BaseViewModel;
import com.example.dell.app.data.LocationApi;
import com.example.dell.app.db.Repository;
import com.example.dell.app.db.model.RouteEntity;
import com.example.dell.app.helpers.DateHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class TrackViewModel extends BaseViewModel {

    private LocationApi locationApi;
    private long startTime = 0;
    private int bestSpeed = 0;
    private int _actualSpeed;
    long _routeTime;
    private RouteEntity routeEntity;
    private float _distance;
    private Location lastLocation = null;
    private final Repository repository;
    private List<Location> _listLocation = new ArrayList<>();

    private MutableLiveData<Location> location = new MutableLiveData<>();
    private MutableLiveData<List<Location>> listLocation = new MutableLiveData<>();
    private MutableLiveData<Integer> actualSpeed = new MutableLiveData<>();
    private MutableLiveData<Float> distance = new MutableLiveData<>();
    private MutableLiveData<String> routeTime = new MutableLiveData<>();
    private MutableLiveData<Boolean> saved = new MutableLiveData<>();

    public MutableLiveData<Boolean> getSaved() {
        return saved;
    }

    public MutableLiveData<String> getRouteTime() {
        return routeTime;
    }

    public MutableLiveData<Location> getLocation() {
        return location;
    }

    public MutableLiveData<Float> getDistance() {
        return distance;
    }

    public MutableLiveData<List<Location>> getListLocation() {
        return listLocation;
    }

    public MutableLiveData<Integer> getActualSpeed() {
        return actualSpeed;
    }

    public TrackViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application.getBaseContext());
    }

    public void locationPermissionGranted() {
        saved.setValue(false);
        setupLocationApi();
        startTimer();
        startLocationUpdates();
    }

    private void startTimer() {
        if (startTime == 0) {
            startTime = System.currentTimeMillis();
        }
        disposables.add(Observable.interval(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it -> setTime()));
    }

    public void saveRoute() {
        routeEntity = new RouteEntity(_distance, bestSpeed, _routeTime);
        disposables.add(repository.insertRoute(routeEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(it ->
                        saveLocationsToFirebase(it))
                .subscribe());

    }

    private void saveLocationsToFirebase(Long routeId) {
        disposables.add(
                repository.insertLocationToFirebase(routeEntity, routeId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnComplete(() ->
                                saved.setValue(true))
                        .subscribe());
    }

    private void setupLocationApi() {
        locationApi = new LocationApi(getApplication().getBaseContext());
        locationApi.setupProvider();
    }

    private void startLocationUpdates() {
        showMyLocation();
        disposables.add(locationApi.getLocationObservable()
                .doOnNext(this::updateLocation)
                .subscribe());
    }

    private void updateLocation(Location newLocation) {
        if (newLocation != null) {
            location.setValue(newLocation);
            _listLocation.add(newLocation);
            if (lastLocation != null) {
                float newDistance = lastLocation.distanceTo(newLocation) / 1000;
                float elapsedTime = newLocation.getTime() - lastLocation.getTime();
                elapsedTime = ((elapsedTime / 1000) / 60 / 60); // from ms->s->m->h
                _actualSpeed = Math.round(newDistance / elapsedTime);
                if (bestSpeed < _actualSpeed) {
                    bestSpeed = _actualSpeed;
                }
                _distance += newDistance;
                distance.setValue(_distance);
                if (_actualSpeed != 0) {
                    actualSpeed.setValue(_actualSpeed);
                }
            }
            lastLocation = newLocation;
        }
    }

    private void setTime() {
        long newTime = System.currentTimeMillis();
        _routeTime = newTime - startTime;
        routeTime.setValue(DateHelper.convertTime(_routeTime));
    }

    private void showMyLocation() {
        Location location = locationApi.getLastKnownLocation();
        if (location != null) {
            updateLocation(location);
        }
    }


}
